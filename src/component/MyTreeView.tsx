import React from 'react';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import { Checkbox, FormControlLabel } from '@mui/material';
import { TreeView, TreeItem } from '@mui/lab';
import { DataType } from 'stor/data';

const MyTreeView: React.FC<{
  data: DataType[];
  expanded: string[];
  checked: Record<string, DataType>;
  indeterminate: Record<string, DataType>;
  setExpanded: (nodeIds: string[]) => void;
  nodeChecked: (nodes: DataType) => void;
}> = ({ data, expanded, checked, setExpanded, nodeChecked, indeterminate }) => {
  const renderTree = (nodes: DataType) => {
    if (!nodes) {
      return null;
    }
    return (
      <TreeItem
        sx={{ width: '98%' }}
        key={nodes.id}
        nodeId={nodes.id}
        label={
          <FormControlLabel
            control={
              <Checkbox
                sx={{ ml: '5px' }}
                indeterminate={indeterminate[nodes.id] && !!indeterminate[nodes.id]}
                checked={!!checked[nodes.id]}
                onChange={(e) => nodeChecked(nodes)}
              />
            }
            onClick={(e) => e.stopPropagation()}
            label={<>{nodes.name}</>}
            key={nodes.id}
          />
        }
      >
        {Array.isArray(nodes.children) ? nodes.children.map((node) => renderTree(node)) : null}
      </TreeItem>
    );
  };

  return (
    <TreeView
      sx={{
        p: 1,
        maxHeight: '800px',
        maxWidth: '850px',
        overflow: 'auto',
        flexGrow: 1,
      }}
      defaultCollapseIcon={<ExpandMoreIcon />}
      defaultExpandIcon={<ChevronRightIcon />}
      expanded={expanded}
      onNodeToggle={(_, nodeIds) => setExpanded(nodeIds)}
    >
      {/* TODO fix for development */}
      {renderTree({ id: 'root', name: 'Parent', children: [...data] })}
    </TreeView>
  );
};

export default MyTreeView;
