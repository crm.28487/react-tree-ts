import React from 'react';
import { TextField, Card } from '@mui/material';
import MyTreeView from './MyTreeView';
import { DataType } from 'stor/data';
import { recalculateSelected, flatNodesTree, indeterminateCalculate } from 'util/filterUtils';
import { debounce } from 'lodash';

const TreeFilter: React.FC<{
  data: DataType;
}> = ({ data }) => {
  const [expanded, setExpanded] = React.useState(['root']);
  const [subjectData, setSubjectData] = React.useState<DataType[]>(data.children || []);
  const [checked, setChecked] = React.useState<Record<string, DataType>>({});
  const [indeterminate, setIndeterminate] = React.useState<Record<string, DataType>>({});

  const filteredNodes = (nodes: DataType[], filter: string): DataType[] => {
    return nodes
      .map((nodes: DataType) => ({
        ...nodes,
        children: nodes.children && filteredNodes(nodes.children, filter),
      }))
      .filter(
        (e) =>
          (e.children && e.children.length) || e.name.toLowerCase().includes(filter.toLowerCase())
      );
  };

  const filterData = (filter: string) => {
    if (filter.length === 0) {
      setSubjectData(data.children || []);
      setExpanded(['root']);
      return;
    }
    // TODO fix for development
    setExpanded(['root', ...flatNodesTree(subjectData).map((o: DataType) => o.id)]);
    const filteredData = data.children && filteredNodes(data.children, filter);
    filteredData && setSubjectData(filteredData);
  };

  const nodeChecked = (node: DataType) => {
    const selected = { ...recalculateSelected(node, subjectData, checked) };
    const indeterminate = { ...indeterminateCalculate(selected) };
    setChecked(selected);
    setIndeterminate(indeterminate);
  };

  return (
    <>
      <TextField
        sx={{ width: '100%' }}
        label="Filter ..."
        onChange={debounce((e) => filterData(e.target.value), 800)}
      />
      <Card sx={{ p: 1, width: '90vh' }}>
        <MyTreeView
          data={subjectData}
          expanded={expanded}
          checked={checked}
          indeterminate={indeterminate}
          nodeChecked={nodeChecked}
          setExpanded={setExpanded}
        />
      </Card>
    </>
  );
};

export default TreeFilter;
