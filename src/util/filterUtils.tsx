import { DataType } from 'stor/data';

export const indeterminateCalculate = (selected: Record<string, DataType>) => {
  const indeterminate = {} as { [key: string]: DataType };
  Object.values(selected).forEach((e) => {
    if (!!e.children) {
      e.children.some((e) => !selected[e.id]) && (indeterminate[e.id] = e);
    }
  });
  return indeterminate;
};

export const flatNodesTree = (data: DataType[]): DataType[] => {
  return data.reduce((acc, nodes) => {
    return [...acc, nodes, ...(nodes.children ? flatNodesTree(nodes.children) : [])];
  }, [] as DataType[]);
};
export const recalculateSelected = (
  clickedNode: DataType,
  data: DataType[],
  selectedNodes: Record<string, DataType>
): Record<string, DataType> => {
  const checked = !(selectedNodes[clickedNode.id] && Object.keys(selectedNodes).length > 1);
  checked ? (selectedNodes[clickedNode.id] = clickedNode) : delete selectedNodes[clickedNode.id];
  return updateParents(
    data,
    updateChildren(clickedNode.children ? clickedNode.children : [], selectedNodes, checked)
  );
};

const updateChildren = (
  data: DataType[],
  clickedNode: { [key: string]: DataType },
  check: boolean
) => {
  data.forEach((node) => {
    if (node.children) {
      clickedNode = updateChildren(node.children, clickedNode, check);
    }
    check ? (clickedNode[node.id] = node) : delete clickedNode[node.id];
  });
  return clickedNode;
};

const updateParents = (data: DataType[], selectedChildren: { [key: string]: DataType }) => {
  data.forEach((node) => {
    if (node.children) {
      selectedChildren = updateParents(node.children, selectedChildren);
      node.children.some((el) => selectedChildren[el.id])
        ? (selectedChildren[node.id] = node)
        : delete selectedChildren[node.id];
    }
    selectedChildren[node.id]
      ? (selectedChildren[node.id] = node)
      : delete selectedChildren[node.id];
  });
  return selectedChildren;
};
