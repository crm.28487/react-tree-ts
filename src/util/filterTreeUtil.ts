import { DataType } from 'stor/data';

// Helper functions for filtering
export const defaultMatcher = (filterText: string, node: DataType) => {
  return node.name.toLowerCase().indexOf(filterText.toLowerCase()) !== -1;
};

export const defaultMatcherId = (id: string, node: DataType) => {
  return node.id === id;
};

export const findNode = (
  node: DataType,
  filter: string,
  matcher: (filter: string, node: DataType) => boolean
): number | boolean | undefined => {
  return (
    matcher(filter, node) || // i match
    (node.children && // or i have decendents and one of them match
      node.children.length &&
      !!node.children.find((child) => findNode(child, filter, matcher)))
  );
};

export function filterTree<T extends DataType>(
  node: T,
  filter: string,
  matcher: (filterText: string, node: DataType) => boolean = defaultMatcher
): T {
  // If im an exact match then all my children get to stay
  if (matcher(filter, node) || !node.children) {
    return node;
  }
  // If not then only keep the ones that match or have matching descendants
  const filtered = node.children
    .filter((child) => findNode(child, filter, matcher))
    .map((child) => filterTree(child, filter, matcher));
  return Object.assign({}, node, { children: filtered });
}

export function filterTreeId<T extends DataType>(
  node: T,
  id: string,
  matcher: (filterText: string, node: DataType) => boolean = defaultMatcher
): T {
  if (node.id === id) {
    return node;
  }
  // If not then only keep the ones that match or have matching descendants
  const filtered =
    node.children &&
    node.children
      .filter((child) => findNode(child, id, matcher))
      .map((child) => filterTreeId(child, id, matcher));
  return Object.assign({}, node, { children: filtered });
}

export function expandFilteredNodes<T extends DataType>(
  node: T,
  filter: string,
  matcher: (filterText: string, node: DataType) => boolean = defaultMatcher
): T {
  let children = node.children;
  if (!children || children.length === 0) {
    return Object.assign({}, node, { toggled: false });
  }
  const childrenWithMatches = node.children
    ? node.children.filter((child) => findNode(child, filter, matcher))
    : [];
  const shouldExpand = childrenWithMatches.length > 0;
  // If im going to expand, go through all the matches and see if thier children need to expand
  if (shouldExpand) {
    children = childrenWithMatches.map((child) => {
      return expandFilteredNodes(child, filter, matcher);
    });
  }
  return Object.assign({}, node, {
    children: children,
    toggled: shouldExpand,
  });
}

let store = [] as string[];
export const getIDsExpandFilter = (node: DataType) => {
  let children = node.children;
  if (!children || children.length === 0) {
    return store;
  }
  if (node.id === 'root') {
    store = ['root'];
  }
  if (children) {
    store.push(children[0].id);
  }
  node.children && node.children.map((child) => getIDsExpandFilter(child));
  return store;
};

/**
 * Find tree item with recursive approach
 * @param node
 * @param id
 */

export function searchTree<T extends DataType>(node: T, id: string): T | null {
  if (node.id === id) {
    return node;
  } else if (node.children != null) {
    let index;
    let result;
    for (index = 0; result == null && index < node.children.length; index++) {
      result = searchTree(node.children[index], id);
    }
    return result as T;
  }
  return null;
}
