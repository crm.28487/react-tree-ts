import { Grid, Box, Typography } from '@mui/material';
import TreeFilter from 'component/TreeFilter';
import { data } from 'stor/data';

export const App = () => {
  return (
    <Grid container justifyContent="center" alignItems="flex-start">
      <Box sx={{ m: 1 }}>
        <Typography variant="h6">Simple Data</Typography>
        <TreeFilter data={data} />
      </Box>
    </Grid>
  );
};

<Grid container direction="row" justifyContent="center" alignItems="flex-start"></Grid>;
